var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "mydb"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  var sql = "Create table customers(id integer(11), name VARCHAR(255))";
  con.query(sql, function(err, result){
	 if(err) throw err;
	 console.log("Table Created!");
  });
});